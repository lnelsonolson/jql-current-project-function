This plugin allows for the use of a currentProject() JQL function in JIRA.

Usage Example:
To find all of the issues with the issuetype of "Task" in the user's "current project".
Query: project = currentProject() AND issuetype = "Task"
