package com.target.jira.plugins.jql.currentproject;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.crowd.embedded.api.User;

import java.util.Collections;
import java.util.List;

/**
* The CurrentProjectFunction class enables the currentProject() JQL function which can be used to search/filter JIRA issues by the last project that the user has selected.
*/

/**
 * A handler for the "currentProject" function. This function will return the last project selected by the user.
 */
public class CurrentProjectFunction extends AbstractJqlFunction
{
    private final UserProjectHistoryManager userProjectHistoryManager;

    public static final String FUNCTION_CURRENT_PROJECT = "currentProject";
    private static final int EXPECTED_ARGS = 0;


    public CurrentProjectFunction(UserProjectHistoryManager userProjectHistoryManager)
    {
        this.userProjectHistoryManager = userProjectHistoryManager;
    }

    /**
     * This method validates the passed in args. In this case the function accepts no args, so let's validate that were none.
     */
    public MessageSet validate(User searcher, FunctionOperand operand, TerminalClause terminalClause)
    {
        return validateNumberOfArgs(operand, EXPECTED_ARGS);
    }
    
    public List<QueryLiteral> getValues(QueryCreationContext queryCreationContext, FunctionOperand operand, TerminalClause terminalClause)
    {
        Project currentProject;
        
        List<QueryLiteral> projectList;
        
        if (queryCreationContext == null || queryCreationContext.getUser() == null)
        {
        	projectList = Collections.emptyList();
        }
        else
        {
        	try{
        		currentProject = userProjectHistoryManager.getCurrentProject(Permissions.BROWSE, queryCreationContext.getUser());
        		
        		projectList = Collections.singletonList(new QueryLiteral(operand, currentProject.getId()));
        	}catch (Exception e) {
        		projectList = Collections.emptyList();
			}
        }
        
        return projectList;
    }

   /**
    * This method returns the min number of args the function takes. In this case - 0
    */
    public int getMinimumNumberOfExpectedArguments()
    {
        return 0;
    }

   /**
    * This method needs to return the type of objects the function deals with. In this case - Projects
    */
    public JiraDataType getDataType()
    {
        return JiraDataTypes.PROJECT;
    }
}